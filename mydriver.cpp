#include <IOKit/IOLib.h>

#include "mydriver.hpp"



// This required macro defines the class's constructors, destructors,

// and several other methods I/O Kit requires.

OSDefineMetaClassAndStructors(com_mydriver, IOService)



// Define the driver's superclass.

#define super IOService



bool com_mydriver::init(OSDictionary *dict)

{
    IOLog("BRUUUH\n");

    bool result = super::init(dict);

    IOLog("Initializing\n");

    return result;

}



void com_mydriver::free(void)

{

    IOLog("Freeing\n");

    super::free();

}



IOService *com_mydriver::probe(IOService *provider,

                                                SInt32 *score)

{

    IOService *result = super::probe(provider, score);

    IOLog("Probing\n");

    return result;

}



bool com_mydriver::start(IOService *provider)

{
    IOLog("BRUUUH2222\n");
    IOPMrootDomain *root = NULL;
    IOReturn        ret=kIOReturnSuccess;

    root = getPMRootDomain();
    bool result = super::start(provider);
    bool currentLidState =  ((OSBoolean *)getPMRootDomain()->getProperty(kAppleClamshellStateKey))->getValue();

    root->receivePowerNotification(kIOPMDisableClamshell | kIOPMPreventSleep);

    IOLog("Starting\n");

    return result;

}



void com_mydriver::stop(IOService *provider)

{
    IOPMrootDomain *root = NULL;
    IOReturn        ret=kIOReturnSuccess;

    root = getPMRootDomain();
    bool currentLidState =  ((OSBoolean *)getPMRootDomain()->getProperty(kAppleClamshellStateKey))->getValue();

    root->receivePowerNotification(kIOPMAllowSleep | kIOPMEnableClamshell);

    IOLog("Stopping\n");

    super::stop(provider);

}
