#!/usr/bin/env bash
    CLAMSHELL=$(ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep | grep Yes)
    echo "running ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep"
    if [ "$CLAMSHELL" ]; then
        sudo cp -R /PATH_TO/XCode/Debug/mydriver.kext /tmp/
        echo "disabling Clamshell Sleep..."
        sudo kextload /tmp/mydriver.kext/
#        log stream --process 0 | grep mydriver
        CLAMSHELL2=$(ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep | grep No)
        if [ "$CLAMSHELL2" = "" ]; then
            sudo kextunload /tmp/mydriver.kext/
            sudo kextload /tmp/mydriver.kext/
            CLAMSHELL2=$(ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep | grep No)
        fi
        echo "==>Current state:  $CLAMSHELL2"
    else
        echo "enabling Clamshell Sleep..."
        sudo kextunload /tmp/mydriver.kext/
#        log stream --process 0 | grep mydriver
        CLAMSHELL2=$(ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep | grep Yes)
        echo "==>Current state: $CLAMSHELL2"
    fi
