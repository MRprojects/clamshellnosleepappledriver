## Disable ClamshellSleep driver for macOS Catalina

Following a hardware problem on my mid-2013 macbookpro I developped a driver for macOS Catalina (kext, c++) to disable the sleep mode when the clasmhell is closed.

Frist, create a new Xcode project and compile the driver.

See ``disableclasmshell.sh`` which loads the driver depending on the current state of the machine

``ioreg -r -k AppleClamshellState | grep AppleClamshellCausesSleep`` gives ``Yes`` or ``No`` depending on whether the driver is loaded.

To (un)load the driver, ``sudo kext(un)load /path_to/mydriver.kext/``


*November 2018*
